require 'spec_helper'
require 'bib'

        
         describe "#Pruebas comparable y enumerable" do
            
            before :all do
                        @libro1 = Libro.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby)", "Pragmatic Bookshelf", "", 4, "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
                        @libro2 = Libro.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby)", "Pragmatic Bookshelf", "", 4, "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
                        @libro3 = Libro.new(["Have Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby)", "Pragmatic Bookshelf", "", 4, "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])

                        @articulo = Articulo.new(["David Flanagan", "Yukihiro Matsumoto"], "The Ruby Programming Language", "2 de Febrero de 2008", ['ISBN-10: 0596516177', "ISBN-13: 978-0596516178"]) 
                        @articulo1 = Articulo.new(["David Flanagan", "Yukihiro Matsumoto"], "The Ruby Programming Language", "2 de Febrero de 2008", ['ISBN-10: 0596516177', "ISBN-13: 978-0596516178"]) 
                        @articulo2 = Articulo.new(["David Rodriguez", "Yukihiro Matsumoto"], "The Ruby Programming Language", "2 de Febrero de 2008", ['ISBN-10: 0596516177', "ISBN-13: 978-0596516178"]) 

			    
            end
            
            
            it "#El libro1 es igual al libro2" do
                expect(@libro1 == @libro2).to eq(true)
            end
        end
